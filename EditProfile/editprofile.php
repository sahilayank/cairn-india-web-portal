<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <!--<meta name="viewport" content="width=device-width, initial-scale=1"> -->

  <script src="../Common/jquery-3.2.1.min.js"></script>
  <script src="editprofile.js"></script>
  <link rel="stylesheet" href="../Common/bootstrap/css/bootstrap.min.css">
  <script src="../Common/bootstrap/css/js/bootstrap.min.js"></script></script>
  <script src="../Common/bootstrap/css/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="signup.css">
  <script src="editprofile.js"></script>
  <title>Edit The Profile</title>
</head>
<body>
  <div class="container">

    <form class="well form-horizontal" action=" " method="post"  id="contact_form">
      <fieldset>

        <!-- Form Name -->
        <legend><center><h2><b>Edit Profile</b></h2></center></legend><br>

        <!-- Text input-->

        <div class="form-group">
          <label class="col-md-4 control-label">First Name</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input  name="first_name" placeholder="The data should be fetched form db" class="form-control"  type="text" required>
            </div>
          </div>
        </div>

        <!-- Text input-->

        <div class="form-group">
          <label class="col-md-4 control-label" >Last Name</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input name="last_name" placeholder="The data should be fetched form db" class="form-control"  type="text" required>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-4 control-label">Department / Office</label>
          <div class="col-md-4 selectContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
              <select name="department" class="form-control selectpicker">
                <option value="">Select your Department/Office</option>
                <option>1.</option>
                <option>2.</option>
                <option >3.</option>
                <option >4.</option>
                <option >5.</option>
                <option >6.</option>

              </select>
            </div>
          </div>
        </div>

        <!-- Text input-->

        <div class="form-group">
          <label class="col-md-4 control-label">Username</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input  name="user_name" placeholder="The data should be fetched form db" class="form-control"  type="text" required>
            </div>
          </div>
        </div>

        <!-- Text input-->

        <div class="form-group">
          <label class="col-md-4 control-label" >Password</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input name="user_password" title="Six or more characters" pattern=".{6,}"  placeholder="The data should be fetched form db" class="form-control"  type="password" required>
            </div>
          </div>
        </div>

        <!-- Text input-->

        <div class="form-group">
          <label class="col-md-4 control-label" >Confirm Password</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input name="confirm_password" title="Six or more characters" placeholder="The data should be fetched form db" class="form-control"  type="password" required>
            </div>
          </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-4 control-label">E-Mail</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
              <input name="email" title="abc@gmail.com"placeholder="The data should be fetched form db" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control"  type="email" required>
            </div>
          </div>
        </div>


        <!-- Text input-->

        <div class="form-group">
          <label class="col-md-4 control-label">Contact No.</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
              <input name="contact_no" title="Enter Ten Digit Mobile Number" pattern="^\d{10}$" placeholder="The data should be fetched form db" class="form-control" type="text" required>
            </div>
          </div>
        </div>

        <!-- Select Basic -->

        <!-- Success message -->
        <!-- <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div> -->

        <!-- Button -->
        <div class="form-group">
          <label class="col-md-4 control-label"></label>
          <div class="col-md-4"><br>
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-warning" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
          </div>
        </div>

      </fieldset>
    </form>
  </div>
</div><!-- /.container -->
</body>
</html>
