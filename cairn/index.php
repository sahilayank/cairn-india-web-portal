 <!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" class="no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ProBusiness Responsive Multipurpose Template</title>
    <meta name="description" content="">

    <!-- CSS FILES -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/flexslider.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen" data-name="skins">
    <link rel="stylesheet" href="css/layout/wide.css" data-name="layout">

    <link rel="stylesheet" href="css/animate.css"/>

    <link rel="stylesheet" type="text/css" href="css/switcher.css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="home">
  <?php include('header.php'); ?>
    <header id="header">

        <!--/#menu-bar -->
        <div class="slider_block" style="padding-right:10%;">
            <div class="flexslider top_slider">
                <ul class="slides">
                    <li class="slide1">
                        <div class="container">
                            <div class="flex_caption1">

                                <p class="slide-heading FromTop">Puneet</p><br/>

                                <p class="sub-line FromBottom">hello how are you  </p><br/>


                            </div>
                            <div class="flex_caption2 FromRight"></div>
                        </div>
                    </li>

                    <li class="slide2">
                        <div class="container">
                            <div class="slide flex_caption1">
                                <p class="slide-heading FromTop">We have done it</p><br/>

                                <p class="sub-line FromRight">Enter text here </p><br/>



                            </div>
                            <div class="flex_caption2 FromBottom"></div>
                        </div>
                    </li>

                    <li class="slide3">
                        <div class="container">
                            <div class="slide flex_caption1">
                                <p class="slide-heading FromTop">Enough for today </p><br/>

                                <p class="sub-line FromRight">We aren't doing more </p><br/>



                            </div>
                            <div class="flex_caption2 FromRight"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </header>
<!--End Header-->

<section class="wrapper">
<!--start info service-->


<!--Start recent work-->
    <section class="latest_work">
        <div class="container">
            <div class="row sub_content">
                <div class="col-md-12">
                    <div class="dividerHeading">
                        <h4><span>Recent Work</span></h4>
                    </div>
                    <div id="recent-work-slider" class="owl-carousel">
                        <div class="recent-item box">

                            <figure class="touching effect-bubba">
                                <img src="images/portfolio/water.jpg" class="img-responsive" alt="" style="height:300px;width:600px"/>


                                <div class="option">
                                    <a href="portfolio_single.html" class="fa fa-link"></a>
                                    <a href="images/portfolio/full/portfolio_1.png" class="fa fa-search mfp-image"></a>
                                </div>
                                <figcaption class="item-description">
                                    <h5>Touch and Swipe</h5>
                                    <p>Mobile</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="recent-item box">
                            <figure class="touching effect-bubba">
                                <img src="images/portfolio/school.jpg" class="img-responsive" alt="" style="height:300px;width:600px"/>
                                <div class="option">
                                    <a href="portfolio_single.html" class="fa fa-link"></a>
                                    <a href="images/portfolio/full/portfolio_2.png" class="fa fa-search mfp-image"></a>
                                </div>
                                <figcaption class="item-description">
                                    <h5>Touch and Swipe</h5>
                                    <p>Mobile</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="recent-item box">
                            <figure class="touching effect-bubba">
                                <img src="images/portfolio/dairy.jpg" class="img-responsive" alt="" style="height:300px;width:600px"/>
                                <!--<div class="option inner">
                                    <div>
                                        <h5>Touch and Swipe</h5>
                                        <a href="images/portfolio/full/portfolio_3.png" class="fa fa-search mfp-image"></a>
                                        <a href="portfolio_single.html" class="fa fa-link"></a>
                                        <span>Mobile</span>
                                    </div>
                                </div>-->
                                <div class="option">
                                    <a href="portfolio_single.html" class="fa fa-link"></a>
                                    <a href="images/portfolio/full/portfolio_3.png" class="fa fa-search mfp-image"></a>
                                </div>
                                <figcaption class="item-description">
                                    <h5>Touch and Swipe</h5>
                                    <p>Mobile</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="recent-item box">
                            <figure class="touching effect-bubba">
                                <img src="images/portfolio/health.jpg" class="img-responsive" alt="" style="height:300px;width:600px"/>
                                <!--<div class="option inner">
                                    <div>
                                        <h5>Touch and Swipe</h5>
                                        <a href="images/portfolio/full/portfolio_4.png" class="fa fa-search mfp-image"></a>
                                        <a href="portfolio_single.html" class="fa fa-link"></a>
                                        <span>Mobile</span>
                                    </div>
                                </div>-->
                                <div class="option">
                                    <a href="portfolio_single.html" class="fa fa-link"></a>
                                    <a href="images/portfolio/full/portfolio_4.png" class="fa fa-search mfp-image"></a>
                                </div>
                                <figcaption class="item-description">
                                    <h5>Touch and Swipe</h5>
                                    <p>Mobile</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="recent-item box">
                            <figure class="touching effect-bubba">
                                <img src="images/portfolio/portfolio_5.png" class="img-responsive" alt="" style="height:300px;width:600px"/>
                                <!--<div class="option inner">
                                    <div>
                                        <h5>Touch and Swipe</h5>
                                        <a href="images/portfolio/full/portfolio_5.png" class="fa fa-search mfp-image"></a>
                                        <a href="portfolio_single.html" class="fa fa-link"></a>
                                        <span>Mobile</span>
                                    </div>
                                </div>-->
                                <div class="option">
                                    <a href="portfolio_single.html" class="fa fa-link"></a>
                                    <a href="images/portfolio/full/portfolio_5.png" class="fa fa-search mfp-image"></a>
                                </div>
                                <figcaption class="item-description">
                                    <h5>Touch and Swipe</h5>
                                    <p>Mobile</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="recent-item box">
                            <figure class="touching effect-bubba">
                                <img src="images/portfolio/portfolio_6.png" class="img-responsive" alt="" style="height:300px;width:600px"/>
                                <!--<div class="option inner">
                                    <div>
                                        <h5>Touch and Swipe</h5>
                                        <a href="images/portfolio/full/portfolio_6.png" class="fa fa-search mfp-image"></a>
                                        <a href="portfolio_single.html" class="fa fa-link"></a>
                                        <span>Mobile</span>
                                    </div>
                                </div>-->
                                <div class="option">
                                    <a href="portfolio_single.html" class="fa fa-link"></a>
                                    <a href="images/portfolio/full/portfolio_6.png" class="fa fa-search mfp-image"></a>
                                </div>
                                <figcaption class="item-description">
                                    <h5>Touch and Swipe</h5>
                                    <p>Mobile</p>
                                </figcaption>
                            </figure>
                        </div>

                        <div class="recent-item box">
                            <figure class="touching effect-bubba">
                                <img src="images/portfolio/portfolio_7.png" class="img-responsive" alt="" style="height:300px;width:600px"/>
                                <div class="option">
                                    <a href="portfolio_single.html" class="fa fa-link"></a>
                                    <a href="images/portfolio/full/portfolio_7.png" class="fa fa-search mfp-image"></a>
                                </div>
                                <figcaption class="item-description">
                                    <h5>Touch and Swipe</h5>
                                    <p>Mobile</p>
                                </figcaption>
                            </figure>
                        </div>

                        <div class="recent-item box">
                            <figure class="touching effect-bubba">
                                <img src="images/portfolio/portfolio_8.png" class="img-responsive" alt="" style="height:300px;width:600px"/>
                                <div class="option">
                                    <a href="portfolio_single.html" class="fa fa-link"></a>
                                    <a href="images/portfolio/full/portfolio_8.png" class="fa fa-search mfp-image"></a>
                                </div>
                                <figcaption class="item-description">
                                    <h5>Touch and Swipe</h5>
                                    <p>Mobile</p>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!--Start recent work-->

<!-- Parallax with Testimonial -->
    <section class="parallax parallax-1">
        <div class="container">
            <!--<h2 class="center">Testimonials</h2>-->
            <div class="row">
                <div id="parallax-testimonial-carousel" class="parallax-testimonial carousel wow fadeInUp">
                    <div class="carousel-inner">
                        <div class="active item">
                            <div class="parallax-testimonial-item">
                                <blockquote>
                                    <p>Dummy text</p>
                                </blockquote>
                                <p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </p>
                                <div class="parallax-testimonial-review">
                                    <img src="images/testimonials/1.png" alt="testimonial">
                                    <span>Cairn India</span>
                                    <small>www.com</small>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="parallax-testimonial-item">
                                <blockquote>
                                    <p>Metus aliquet tincidunt metus, sit amet mattis lectus sodales ac. Suspendisse rhoncus dictum eros, ut egestas eros luctus eget. Nam nibh sem, mattis et feugiat ut, porttitor nec risus.</p>
                                </blockquote>
                                <p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </p>
                                <div class="parallax-testimonial-review">
                                    <img src="images/testimonials/2.png" alt="testimonial">
                                    <span>Jonathan Dower</span>
                                    <small>Leopard</small>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="parallax-testimonial-item">
                                <blockquote>
                                    <p>Nunc aliquet tincidunt metus, sit amet mattis lectus sodales ac. Suspendisse rhoncus dictum eros, ut egestas eros luctus eget. Nam nibh sem, mattis et feugiat ut, porttitor nec risus.</p>
                                </blockquote>
                                <p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </p>
                                <div class="parallax-testimonial-review">
                                    <img src="images/testimonials/3.png" alt="testimonial">
                                    <span>Jonathan Dower</span>
                                    <small>Leopard</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ol class="carousel-indicators">
                        <li data-slide-to="0" data-target="#parallax-testimonial-carousel" class=""></li>
                        <li data-slide-to="1" data-target="#parallax-testimonial-carousel" class="active"></li>
                        <li data-slide-to="2" data-target="#parallax-testimonial-carousel" class=""></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
<!-- end : Parallax with Testimonial -->



</section><!--end wrapper-->

<!--start footer-->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="widget_title">
                    <h4><span>About Us</span></h4>
                </div>
                <div class="widget_content">
                    <p>Donec earum rerum hic tenetur ans sapiente delectus, ut aut reiciendise voluptat maiores alias consequaturs aut perferendis doloribus asperiores.</p>
                    <ul class="contact-details-alt">
                        <li><i class="fa fa-map-marker"></i> <p><strong>Address</strong>: #2021 Lorem Ipsum</p></li>
                        <li><i class="fa fa-user"></i> <p><strong>Phone</strong>:(+91) 9000-12345</p></li>
                        <li><i class="fa fa-envelope"></i> <p><strong>Email</strong>: <a href="#">mail@example.com</a></p></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="widget_title">
                    <h4><span>Recent Posts</span></h4>
                </div>
                <div class="widget_content">
                    <ul class="links">
                        <li> <a href="#">Aenean commodo ligula eget dolor<span>November 07, 2015</span></a></li>
                        <li> <a href="#">Temporibus autem quibusdam <span>November 05, 2015</span></a></li>
                        <li> <a href="#">Debitis aut rerum saepe <span>November 03, 2015</span></a></li>
                        <li> <a href="#">Et voluptates repudiandae <span>November 02, 2015</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="widget_title">
                    <h4><span>Twitter</span></h4>

                </div>
                <div class="widget_content">
                    <ul class="tweet_list">
                        <li class="tweet_content item">
                            <p class="tweet_link"><a href="#">@jquery_rain </a> Lorem ipsum dolor et, consectetur adipiscing eli</p>
                            <span class="time">29 September 2015</span>
                        </li>
                        <li class="tweet_content item">
                            <p class="tweet_link"><a href="#">@jquery_rain </a> Lorem ipsum dolor et, consectetur adipiscing eli</p>
                            <span class="time">29 September 2015</span>
                        </li>
                        <li class="tweet_content item">
                            <p class="tweet_link"><a href="#">@jquery_rain </a> Lorem ipsum dolor et, consectetur adipiscing eli</p>
                            <span class="time">29 September 2015</span>
                        </li>
                    </ul>
                </div>
                <div class="widget_content">
                    <div class="tweet_go"></div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="widget_title">
                    <h4><span>Flickr Gallery</span></h4>
                </div>
                <div class="widget_content">
                    <div class="flickr">
                        <ul id="flickrFeed" class="flickr-feed"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--end footer-->

<section class="footer_bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <p class="copyright">&copy; Copyright 2015 ProBusiness | Powered by  <a href="http://www.jqueryrain.com/">jQuery Rain</a></p>
            </div>

            <div class="col-sm-6 ">
                <div class="footer_social">
                    <ul class="footbot_social">
                        <li><a class="fb" href="#." data-placement="top" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a class="twtr" href="#." data-placement="top" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a class="dribbble" href="#." data-placement="top" data-toggle="tooltip" title="Dribbble"><i class="fa fa-dribbble"></i></a></li>
                        <li><a class="skype" href="#." data-placement="top" data-toggle="tooltip" title="Skype"><i class="fa fa-skype"></i></a></li>
                        <li><a class="rss" href="#." data-placement="top" data-toggle="tooltip" title="RSS"><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/retina-1.1.0.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script> <!-- jQuery cookie -->
<script type="text/javascript" src="js/styleswitch.js"></script> <!-- Style Colors Switcher -->
<!--
<script src="js/jquery.fractionslider.js" type="text/javascript" charset="utf-8"></script>
-->
<script type="text/javascript" src="js/jquery.smartmenus.min.js"></script>
<script type="text/javascript" src="js/jquery.smartmenus.bootstrap.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/jflickrfeed.js"></script>
<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="js/swipe.js"></script>

<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>

<script src="js/main.js"></script>




    <script>
        $('.flexslider.top_slider').flexslider({
            animation: "fade",
            controlNav: false,
            directionNav: true,
            prevText: "&larr;",
            nextText: "&rarr;"
        });
    </script>

    <!-- WARNING: Wow.js doesn't work in IE 9 or less -->
    <!--[if gte IE 9 | !IE ]><!-->
        <script type="text/javascript" src="js/wow.min.js"></script>
        <script>
            // WOW Animation
            new WOW().init();
        </script>
    <![endif]-->

</body>
</html>
